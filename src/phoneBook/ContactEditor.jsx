import React from "react";
import { Link } from "react-router-dom";
import '../addContact/AddContact.less'
import FormInput from "./FormInput";

class ContactEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props.contact == null
			? { name: "", phone: "", title: 'Create new contact' }
			: {
				name: props.contact.name,
				phone: props.contact.phone,
				title: `Edit contact`
			};
		this._handleUpdateContact = this._handleUpdateContact.bind(this);
		this._handleChange = this._handleChange.bind(this);
	}
	_handleUpdateContact() {
		this.props.onSubmit({ ...this.state }, this.props.contact);
	}

	_handleChange(id, event) {
		let validName =/^.{0,25}$/;
		let validPhone =/^\d{0,11}$/;

		if(validName.test(event) && id==='name' ){
			this.setState({name: event})
		}
		if(validPhone.test(event) && id==='phone'){
			this.setState({phone: event})
		}}

	render() {
		const disabled = this.state.phone  === "";

		return <fieldset>
			<h1>{this.state.title}</h1>
			<FormInput fieldId="name" placeholder="name" value={this.state.name} onChange={this._handleChange} />
			<FormInput fieldId="phone"
					   placeholder="Phone"
					   value={this.state.phone} onChange={this._handleChange} />
			<div>
				<Link to="/">
					<button className="button"><p>Cancel</p></button>
				</Link>
				<Link to="/">
					<button className={`${disabled ? "nonActive" : "button" }`}
							disabled={disabled}
							onClick={this._handleUpdateContact}><p>OK</p></button>
				</Link>
			</div>
		</fieldset>
	}
}

export default ContactEditor;