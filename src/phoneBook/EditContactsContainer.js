import { connect } from "react-redux";
import ContactEditor from "./ContactEditor";
import { confirmEditContact } from "./ContactsActionCreators";
import { getSelectedContact } from "./ContactsSelector";


const mapStateToProps = state => {
	return {
		contact: getSelectedContact(state.contacts)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onSubmit: (newContact, oldContact) => {dispatch(confirmEditContact(newContact, oldContact))},
	}
};

const EditContactContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(ContactEditor);

export default EditContactContainer;