import React from "react";
import { Link } from "react-router-dom";
import { store } from "../store";
import { loadPhones } from "./ContactsActionCreators";
import PhoneNumber from "./PhoneNumber";

const PhoneList = (props) => {
	const { data, selected, onSelect } = props;
	return data.map((contact, index) => {
		const isSelected = !!selected && contact.name === selected.name && contact.phone === selected.phone;
		const selectedValue = isSelected ? null : contact;
		return <PhoneNumber data={contact} onSelect={() => onSelect(selectedValue)} key={index} selected={isSelected}/>;
	});
};


class Contacts extends React.Component {
	constructor(props) {
		super(props);
		this._handleSearch = this._handleSearch.bind(this);
		this._handlerDelete = this._handlerDelete.bind(this);
		this._handlerSelect = this._handlerSelect.bind(this);
	}

	_handleSearch(event) {
		this.props.onSearch(event.target.value)
	}

	_handlerDelete(contact) {
		this.props.onRemove(contact)
	}

	componentDidMount() {
		store.dispatch(loadPhones());
	}

	_handlerSelect(contact) {
		this.props.onSelect(contact)
	}

	render() {
		const disabled = !this.props.selected;
		return <div>
			<div className="search">
				<input className="form-control" placeholder="search"
					   onChange={this._handleSearch} type="text" value={this.props.filter}/>
			</div>
			<PhoneList className="phone_list" data={this.props.data} onSelect={this._handlerSelect}
					   selected={this.props.selected}/>
			<div className="toolbar">
				<Link to="/add">
					<button type="button" className="button">Add</button>
				</Link>
				<Link to="/edit">
					<button type="button"
							className={`${disabled ? "nonActive" : "button" }`}
							disabled={disabled}>Edit</button>
				</Link>
				<button type="button"
						onClick={this._handlerDelete}
						className={`${disabled ? "nonActive" : "button" }`}
						disabled={disabled}>
					Delete
				</button>
			</div>
		</div>
	}
}

export default Contacts;