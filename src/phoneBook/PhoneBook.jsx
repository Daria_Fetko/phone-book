import React from 'react';
import DeleteContainer from "../deletePhone/DeleteContainer";

import ContactsContainer from "./ContactsContainer";

class PhoneBook extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return <fieldset>
			<h1>My Contacts</h1>
			<ContactsContainer/>
			<DeleteContainer />
		</fieldset>
	};


}

export default PhoneBook;