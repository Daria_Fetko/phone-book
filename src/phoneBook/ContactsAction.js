export const CONTACTS_IS_LOAD = "CONTACTS_IS_LOAD";
export const DELETE_PHONE = "DELETE_PHONE";
export const ADD_PHONE = "ADD_PHONE";
export const SEARCH_PHONE = "SEARCH_PHONE";
export const OPEN_DELETE_MODAL = "OPEN_DELETE_MODAL";
export const CLOSE_DELETE_MODAL = "CLOSE_DELETE_MODAL";
export const SELECTED_CONTACT = "SELECTED_CONTACT";
export const EDIT_CONTACT = "EDIT_CONTACT";
export const EXIT_EDIT_CONTACT = "EXIT_EDIT_CONTACT";
