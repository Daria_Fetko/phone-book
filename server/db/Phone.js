// const sqlite3 = require("sqlite3").verbose();
// const dbName = "phones.sqlite";
// const db = new sqlite3.Database(dbName);

const phones = {
	"Yurii": {name: "Yurii", phone: "34785165549"},
	"Lera": {name: "Lera", phone: "56456156165"},
	"Dasha": {name: "Dasha", phone: "654548944645"}
};

class Phone {
	static all(cb) {
		cb(null, phones);
	}

	static get(name, cb) {
		cb(null, phones[name]);
	}

	static update(phone, cb) {
		if(!phones[phone.name]) {
			cb("Not existing phone");
		}
		phones[phone.name] = phone;
		cb(null);
	}

	static create(phone, cb) {
		if(phones[phone.name]) {
			cb("Already existing phone");
		}
		phones[phone.name] = phone;
		cb(null);
	}

	static delete(name, cb) {
		delete phones[name];
		cb(null);
	}
}

module.exports = Phone;