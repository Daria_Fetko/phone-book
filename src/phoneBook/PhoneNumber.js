import React from "react";
import './PhoneBook.less'
import { ListGroupItem } from "react-bootstrap";

class PhoneNumber extends React.Component {
	constructor(props) {
		super(props);
		this._handlerChoose = this._handlerChoose.bind(this)
	}

	_handlerChoose = () => {
		this.props.onSelect();
	};

	render() {
		let classToApply = this.props.selected ? 'info' : null;

		return <ListGroupItem bsStyle={classToApply}
							  onClick={this._handlerChoose}
							  header={this.props.data.name}>{this.props.data.phone}</ListGroupItem>

	}

}


export default PhoneNumber;