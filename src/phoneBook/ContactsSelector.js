export const getContactsData = state => state.filteredContacts;

export const filteredContacts = (state, filter) => state.contacts.filter((val) =>
	val.name.includes(filter) || val.phone.includes(filter));

export const getContactsFilter = state => state.filter;

export const getConfirmDelete = state => state.delete;

export const getSelectedContact = state => state.selected;
