import { connect } from "react-redux";
import { getConfirmDelete, getSelectedContact } from "../phoneBook/ContactsSelector";
import { cancelDeleteContact, confirmDeleteContact } from "./DeleteActionCreators";

import modalConformation from "./DeletePhone";

const mapStateToProps = state =>  {
	return {
		show: getConfirmDelete(state),
		contact: getSelectedContact(state.contacts)
	}
};

const mapDispatchToProps = dispatch => {
	return {
		onCancel: () => {dispatch(cancelDeleteContact())},
		onConfirmDelete: (contact) => {dispatch(confirmDeleteContact(contact))},
	}
};

const DeleteContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(modalConformation);

export default DeleteContainer;