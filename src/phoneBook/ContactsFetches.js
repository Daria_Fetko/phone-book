import { PHONES_ALL_URL, PHONES_URL } from "./contactURL";


export const fetchPhones = () => fetch(PHONES_ALL_URL).then(response => response.json());

export const savePhone = (contact) => fetch(PHONES_URL, {
	headers: {
		"Content-Type": "application/json"
	},
	method: "POST",
	body: JSON.stringify(contact)
}).then(response => response.json());


export const deletePhone = (contact) => fetch(`${PHONES_URL}/${contact.name}`, {
	headers: {
		"Content-Type": "application/json"
	},
	method: "DELETE",
});

export const updateContact = (contact) => fetch(PHONES_URL, {
	headers: {
		"Content-Type": "application/json"
	},
	method: "PUT",
	body: JSON.stringify(contact)
});