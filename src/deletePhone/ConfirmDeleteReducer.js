import { CLOSE_DELETE_MODAL, OPEN_DELETE_MODAL, DELETE_PHONE } from "../phoneBook/ContactsAction";

const isOpen = false;

const confirmDeleteReducer = (state = isOpen, action) => {
	switch (action.type) {
		case OPEN_DELETE_MODAL:
			return action.payload;
		case CLOSE_DELETE_MODAL:
			return action.payload;
		default:
			return state;
	}
};

export default confirmDeleteReducer;



