import { fetchPhones, updateContact } from "./ContactFetchesLocal";
import { CONTACTS_IS_LOAD, EDIT_CONTACT, EXIT_EDIT_CONTACT, SEARCH_PHONE, SELECTED_CONTACT } from "./ContactsAction";


export const loadPhones = () => {
	return (dispatch) => {
		fetchPhones().then(data => dispatch({
			type: CONTACTS_IS_LOAD,
			payload: data,
		}));
	};
};

export const searchContact = value => ({
	type: SEARCH_PHONE,
	payload: value
});

export const selectContact = (contact) => ({
	type: SELECTED_CONTACT,
	payload: contact
});


export const confirmEditContact = (newContact, oldContact) => {
	return (dispatch) => {
		updateContact(newContact).then(() => {
			dispatch({
				type: EDIT_CONTACT,
				payload: {
					updated: newContact,
					oldContact: oldContact
				}
			});
			dispatch({
				type: EXIT_EDIT_CONTACT,
				payload: false
			});
		}, () => console.log("error"));
	}
};