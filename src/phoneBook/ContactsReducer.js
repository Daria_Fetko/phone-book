import {
	ADD_PHONE, CONTACTS_IS_LOAD, DELETE_PHONE, EDIT_CONTACT, SEARCH_PHONE, SELECTED_CONTACT
} from "./ContactsAction";
import { filteredContacts } from "./ContactsSelector";


const initialState = {
	contacts: [],
	filteredContacts: [],
	filter: "",
	selected: null
};

const contactsReducer = (state = initialState, action) => {
	switch (action.type) {
		case CONTACTS_IS_LOAD:
			return { ...state, contacts: [...action.payload], filteredContacts: [...action.payload] };
		case SEARCH_PHONE:
			return { ...state, filter: action.payload, filteredContacts: filteredContacts(state, action.payload) };
		case ADD_PHONE: {
			const updatedContacts = [...state.contacts, action.payload];
			const updatedState = { ...state, contacts: updatedContacts };
			return { ...updatedState,
				filteredContacts: filteredContacts(updatedState, state.filter) };
		}
		case SELECTED_CONTACT:
			return { ...state, selected: action.payload };
		case DELETE_PHONE: {
			const index = state.contacts.indexOf(action.payload);
			const updatedContacts = [...state.contacts];
			updatedContacts.splice(index, 1);
			const updatedState = { ...state, contacts: updatedContacts };
			return { ...updatedState, filteredContacts: filteredContacts(updatedState, state.filter), selected: null };
		}
		case EDIT_CONTACT:
			const index = state.contacts.indexOf(action.payload.oldContact);
			const updatedContacts = [...state.contacts];
			updatedContacts[index] = action.payload.updated;
			const updatedState = { ...state, contacts: updatedContacts };
			return { ...updatedState, filteredContacts: filteredContacts(updatedState, state.filter) };
		default:
			return state;
	}
};

export default contactsReducer;
