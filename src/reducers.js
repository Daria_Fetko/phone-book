import { combineReducers } from "redux";
import confirmDeleteReducer from "./deletePhone/ConfirmDeleteReducer";

import contactsReducer from "./phoneBook/ContactsReducer";


export default combineReducers({
	contacts: contactsReducer,
	delete: confirmDeleteReducer
})
