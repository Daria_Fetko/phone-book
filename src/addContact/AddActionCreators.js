import { savePhone } from "../phoneBook/ContactFetchesLocal";
import { ADD_PHONE } from "../phoneBook/ContactsAction";

export const addNewContact = (contact) => {
	return (dispatch) => {
		savePhone(contact).then( result => {
		dispatch({
					 type: ADD_PHONE,
					 payload: result,
				 });
	});
}};
