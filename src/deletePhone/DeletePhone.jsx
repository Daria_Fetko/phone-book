import React from "react";
import { Button, Modal } from "react-bootstrap";


class DeletePhone extends React.Component {
	constructor(props) {
		super(props);
		this._handlerCancel = this._handlerCancel.bind(this);
		this._handlerConfirm = this._handlerConfirm.bind(this)
	}

	_handlerCancel() {
		this.props.onCancel()
	}

	_handlerConfirm() {
		this.props.onConfirmDelete(this.props.contact)
	}

	render() {

		return this.props.contact ? <Modal show={this.props.show}>
			<Modal.Header closeButton>
				<Modal.Title>Are you sure that you want delete {this.props.contact.name} contact?</Modal.Title>
			</Modal.Header>
			<Modal.Footer>
				<Button onClick={this._handlerCancel}>Cancel</Button>
				<Button onClick={this._handlerConfirm}>Delete</Button>
			</Modal.Footer>
		</Modal> : null;

	}
}


export default DeletePhone