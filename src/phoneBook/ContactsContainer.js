import { connect } from "react-redux";
import { startDeleteContact } from "../deletePhone/DeleteActionCreators";
import Contacts from "./Contacts";
import { searchContact, selectContact } from "./ContactsActionCreators";
import { getContactsData, getContactsFilter, getSelectedContact } from "./ContactsSelector";



const mapStateToProps = state => {
	return {
		data: getContactsData(state.contacts),
		filter: getContactsFilter(state.contacts),
		selected: getSelectedContact(state.contacts)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onSearch: (contact) => {dispatch(searchContact(contact))},
		onRemove: (show) => {dispatch(startDeleteContact(show))},
		onSelect: (contact) => {dispatch(selectContact(contact))}
	}
};

const ContactsContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Contacts);

export default ContactsContainer;