const express = require("express");
const bodyParser = require("body-parser");
const errorHandler = require("./errorHandler");
const app = express();

const Phone = require("./db/Phone");

const port = 9080;

app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET,HEAD,POST,PUT,DELETE");
	next();
});


app.get('/', (req, res) => {
	res.send('Hello World');
});

app.get("/phones/all", (req, res, next) => Phone.all((err, phones) => {
	if (err) {
		return next(err);
	}
	const values = Object.keys(phones).map(k => phones[k]);
	res.send(values);
}));

app.get("/phones/:name", (req, res, next) => {
	Phone.get(req.params.name, (err, phone) => {
		if (err) {
			return next(err);
		}
		res.send(phone);
	});
});

app.put("/phones", (req, res, next) => {
	Phone.update(req.body, (err) => {
		if (err) {
			return next(err);
		}
		res.send(req.body);
	});
});

app.post("/phones", (req, res, next) => {
	Phone.create(req.body, (err) => {
		if (err) {
			return next(err);
		}
		res.send(req.body);
	});
});

app.delete("/phones/:name", (req, res, next) => {
	Phone.delete(req.params.name, (err) => {
		if (err) {
			return next(err);
		}
	});
	res.sendStatus(200);
});


app.use(errorHandler);

app.listen(port);