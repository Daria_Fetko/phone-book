import { deletePhone } from "../phoneBook/ContactFetchesLocal";
import { CLOSE_DELETE_MODAL, OPEN_DELETE_MODAL, DELETE_PHONE } from "../phoneBook/ContactsAction";



export const startDeleteContact = () => {
	return {
		type: OPEN_DELETE_MODAL,
		payload: true
	};
};

export const cancelDeleteContact = () => {
	return {
		type: CLOSE_DELETE_MODAL,
		payload: false
	};
};


export const confirmDeleteContact = (contact) => {
	return (dispatch) => {
		deletePhone(contact).then(() => {
			dispatch({
				type: DELETE_PHONE,
				payload: contact
			});
			dispatch({
				type: CLOSE_DELETE_MODAL,
				payload: false
			});
		}, () => console.log("error"));
	}
};