import _ from "lodash";

const KEY = "phones";

const getPhones = () => JSON.parse(sessionStorage.getItem(KEY) || "{}");

export const fetchPhones = () => new Promise(resolve => {
	const values  = resolve(_.values(getPhones()));
	return values;
});

export const savePhone = (contact) => {
	sessionStorage.setItem(KEY, JSON.stringify({...getPhones(), [contact.name]: contact}));
	return new Promise(resolve => resolve(contact));
};


export const deletePhone = (contact) => {
	sessionStorage.setItem(KEY, JSON.stringify(delete {...getPhones().contact}));
	return new Promise(resolve => resolve(contact));
};

export const updateContact = (contact) => {
	sessionStorage.setItem(KEY, JSON.stringify({...getPhones(), [contact.name]: contact}));
	return new Promise(resolve => resolve(contact));
};