import React from "react";

export default class FormInput extends React.Component {
	constructor(props) {
		super(props);
		this._handleChange = this._handleChange.bind(this);
	}

	_handleChange(event) {
		this.props.onChange(this.props.fieldId, event.target.value );
	}

	render() {
		return <input className="form-control" type="text" value={this.props.value}
					  placeholder={this.props.fieldId}
					  onChange={this._handleChange} disabled={this.props.disabled}/>;
	}
}
