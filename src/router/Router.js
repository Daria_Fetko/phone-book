import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import AddContactContainer from "../addContact/AddContactContainer";
import EditContactContainer from "../phoneBook/EditContactsContainer";
import PhoneBook from "../phoneBook/PhoneBook";

const Routes = () => (
	<HashRouter>
		<Switch>
			<Route exact path="/" component={PhoneBook}/>
			<Route path="/add" component={AddContactContainer}/>
			<Route path="/edit" component={EditContactContainer}/>
		</Switch>
	</HashRouter>
);

export default Routes;