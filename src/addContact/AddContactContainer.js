import { connect } from "react-redux";
import ContactEditor from "../phoneBook/ContactEditor";
import { addNewContact } from "./AddActionCreators";


const mapDispatchToProps = dispatch => {
	return {
		onSubmit: (contact) => {dispatch(addNewContact(contact))},
	}
};

const AddContactContainer = connect(
	null,
	mapDispatchToProps
)(ContactEditor);

export default AddContactContainer;